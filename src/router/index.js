import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "absence-management",
    component: () => import("@/views/AbsenceManagement.vue"),
    meta: {
      title: "AbsenceManagement",
      layout: "DashboardLayout",
    },
  },
  {
    path: "/notice-board",
    name: "notice-board",
    component: () => import("@/views/NoticeBoard.vue"),
    meta: {
      title: "NoticeBoard",
      layout: "DashboardLayout",
    },
  },
  {
    path: "/404",
    name: "PageNotFound",
    component: () => import("@/views/PageNotFound.vue"),
  },
  { path: "*", redirect: "/404" },
];

const router = new VueRouter({
  linkActiveClass: "active", // active class for non-exact links.
  linkExactActiveClass: "active", // active class for *exact* links.
  mode: "history",
  routes,
});

export default router;
