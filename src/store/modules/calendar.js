import * as types from "../mutation-types";
import calendarData from "@assets/data/calendarData.json";

export default {
  namespaced: true,
  state() {
    return {
      events: calendarData,
      activeDate: new Date(),
      clickedDate: null,
      dateShownRange: {},
      filter: null,
    };
  },
  getters: {
    events(state) {
      if (state.filter) {
        return state.events.filter((x) => {
          if (state.filter === "upcoming-absence") {
            return (
              x["event-type"] === "annual-leave" ||
              x["event-type"] === "sick-day"
            );
          } else if (state.filter === "annual-leave") {
            return x["event-type"] === "annual-leave";
          } else if (state.filter === "public-holiday") {
            return x["event-type"] === "public-holiday";
          } else if (state.filter === "sick-day") {
            return x["event-type"] === "sick-day";
          } else if (state.filter === "requests") {
            return (
              x["event-type"] === "annual-leave" ||
              x["event-type"] === "sick-day"
            );
          }
        });
      }

      return state.events;
    },

    activeDate(state) {
      return state.activeDate;
    },

    publicHolidays(state, getters) {
      if (!Object.entries(state.dateShownRange).length) {
        return 0;
      }

      return getters.activeMonthDays.filter(
        (x) => x["event-type"] === "public-holiday"
      ).length;
    },

    sickDays(state, getters) {
      if (!Object.entries(state.dateShownRange).length) {
        return 0;
      }

      return getters.activeMonthDays.filter(
        (x) => x["event-type"] === "public-holiday"
      ).length;
    },

    pendingDays(state, getters) {
      if (!Object.entries(state.dateShownRange).length) {
        return 0;
      }

      return getters.activeMonthDays.filter(
        (x) => x["event-status"] === "pending"
      ).length;
    },

    totalDaysSpent(state) {
      return state.events.filter((x) => x["event-type"] === "public-holiday")
        .length;
    },

    activeMonthDays(state) {
      return state.events.filter((x) => {
        const date = new Date(x["dates"][0]);
        return (
          date.getTime() <= state.dateShownRange.last.getTime() &&
          date.getTime() >= state.dateShownRange.first.getTime()
        );
      });
    },

    clickedDate(state) {
      return state.clickedDate;
    },
  },
  mutations: {
    [types.UPDATE_FILTER](state, payload) {
      state.filter = payload;
    },

    [types.UPDATE_DATE_SHOWN_RANGE](state, payload) {
      state.dateShownRange = payload;
    },

    [types.UPDATE_CLICKED_DATE](state, payload) {
      state.clickedDate = payload;
    },

    [types.DELETE_EVENT](state, payload) {
      state.events.splice(payload, 1);

      state.clickedDate = null;
    },

    [types.ADD_EVENT](state, payload) {
      state.events.push(payload);
    },

    [types.UPDATE_EVENT](state, payload) {
      state.events[payload.index]["event-name"] = payload.payload.name;
      state.events[payload.index]["event-type"] = payload.payload.type;
      state.events[payload.index]["event-note"] = payload.payload.note;

      state.events[payload.index]["dates"] = payload.payload.dates;

      let newDate = new Date(state.events[payload.index].dates[0]);

      let newClickedDate = {
        data: state.events[payload.index],
        date: newDate,
        year: newDate.getFullYear(),
        month: newDate.getMonth(),
        day: newDate.getDate(),
      };

      state.clickedDate = newClickedDate;
    },
  },
  actions: {
    updateFilter(context, payload) {
      context.commit(types.UPDATE_FILTER, payload);
    },

    updateDateShownRange(context, payload) {
      context.commit(types.UPDATE_DATE_SHOWN_RANGE, payload);
    },

    updateClickedDate(context, payload) {
      context.commit(types.UPDATE_CLICKED_DATE, payload);
    },

    addEvent(context, payload) {
      let event = {};
      event["event-name"] = payload.name;
      event["event-status"] = "pending";
      event["event-note"] = payload.note;

      if (payload.type === "Annual Leave") {
        event["event-type"] = "annual-leave";
      } else {
        event["event-type"] = "sick-day";
      }

      let dates = [];

      let currentDate = new Date(payload.fromDate);
      let endDate = new Date(payload.toDate);

      while (currentDate <= endDate) {
        dates.push(currentDate.toISOString().slice(0, 10));
        currentDate.setDate(currentDate.getDate() + 1);
      }

      event["dates"] = dates;

      context.commit(types.ADD_EVENT, event);
    },

    deleteEvent({ commit, state }, payload) {
      const index = state.events.findIndex((x) => x["dates"] === payload.dates);

      if (index === -1) {
        throw new Error("There was a problem while deleting the event");
      }

      commit(types.DELETE_EVENT, index);
    },

    updateEvent({ commit, state }, payload) {
      const index = state.events.findIndex((x) => x["dates"] === payload.dates);

      if (index === -1) {
        throw new Error("There was a problem while updating the event");
      }

      let newPayload = { ...payload };

      if (newPayload.type === "Annual Leave") {
        newPayload.type = "annual-leave";
      } else {
        newPayload.type = "sick-day";
      }

      let dates = [];

      let currentDate = new Date(payload.fromDate);
      let endDate = new Date(payload.toDate);

      while (currentDate <= endDate) {
        dates.push(currentDate.toISOString().slice(0, 10));
        currentDate.setDate(currentDate.getDate() + 1);
      }

      newPayload["dates"] = dates;

      commit(types.UPDATE_EVENT, { index: index, payload: newPayload });
    },
  },
};
