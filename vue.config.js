var path = require("path");

module.exports = {
  transpileDependencies: ["vuetify"],
  configureWebpack: {
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src/"),
        "~": path.resolve(__dirname, "src/"),
        "@assets": path.resolve(__dirname, "src/assets"),
      },
    },
  },
};
