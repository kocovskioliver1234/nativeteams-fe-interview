import Vue from "vue";
import Vuex from "vuex";
import noticeboard from "@/store/modules/noticeboard";
import users from "@/store/modules/users";
import calendar from "@/store/modules/calendar";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    noticeboard,
    users,
    calendar,
  },
});
