import users from "@assets/data/usersSimpleData.json";

export default {
  namespaced: true,
  state() {
    return {
      users: users,
      activeUser: {
        totalAllowance: 24,
      },
    };
  },
  getters: {
    users(state) {
      return state.users;
    },

    activeUser(state) {
      return state.activeUser;
    },
  },
};
