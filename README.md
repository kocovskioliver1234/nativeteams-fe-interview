![Logo](https://nativeteams.com/images/logo-native-teams.svg)

# Native Team's Front End Challenge

In this challenge, you will be creating a single-page application (SPA) using Vue.js 2.0, Vuex, and Vue Router.

Design link: https://www.figma.com/file/EsPVmYXuVu0G1ECh0I1MVm/Native-Teams-FE-Challenge

## Conditions
1. The challenge is to be finished within 1 week from the date you got the link in your email.
2. When I clone your repository, I should be able to run the project and see the result.
3. The Calendar Panel component should be created from scratch (don't use vuetify on that).

## Requirements

1. The SPA should have two pages: Absence Management and Noticeboard.
2. You need to recreate the designs we provided for both pages (100% of what the design looks like).
3. Make it as responsive as possible.
4. The form submission should be stored in Vuex store and should be fetched into the page.
5. The SPA should have navigation between the pages using Vue Router.
6. The SPA should have a 404 page.

## Evaluation Criteria
1. Code structure and organization.
2. Use of Vuex and Vue Router.
3. User interface and user experience.
4. Use of best practices and design patterns.
5. Handling of errors and edge cases.

## TIPS
1. Use Vuetify's component to implement what you need faster.
2. Use bootstrap classes for styling to make it faster and responsive.
3. We don't care how you get the data do whatever feels best for you.

## Submission
Deadline is 1 week after getting the repository link.

Please fork this repository from Gitlab https://gitlab.com/lalagias/nativeteams-fe-interview. 
Invite @lalagias to your forked project when it's finished. 

## Project Setup

Install with npm

```bash
  npm install
```

Compiles and hot-reloads for development

```bash
  npm run serve
```

Compiles and minifies for production

```bash
  npm run build
```

## Tech Stack

**Client:** Vue 2, Vuex, VueRouter

**Libraries:** vue-bootstrap, vuetify (v2)


## Feedback

If you have any feedback, please reach out to Dimitris (dimitris@nteams.com).
