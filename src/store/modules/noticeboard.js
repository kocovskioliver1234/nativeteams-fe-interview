import * as types from "../mutation-types";

export default {
  namespaced: true,
  state() {
    return {
      notices: [],
    };
  },
  getters: {
    notices(state) {
      return state.notices;
    },
  },
  mutations: {
    [types.ADD_NOTICE](state, payload) {
      state.notices.push(payload);
    },

    [types.UPDATE_NOTICE](state, payload) {
      state.notices[payload.index].title = payload.notice.title;
      state.notices[payload.index].message = payload.notice.message;
      state.notices[payload.index].attachments = payload.notice.attachments;
    },

    [types.DELETE_NOTICE](state, payload) {
      state.notices.splice(payload, 1);
    },

    [types.SET_READ](state, payload) {
      state.notices[payload].unread = 0;
    },
  },
  actions: {
    addNotice(context, payload) {
      context.commit(types.ADD_NOTICE, payload);
    },

    updateNotice({ commit, state }, payload) {
      const index = state.notices.findIndex((x) => x.id === payload.id);

      if (index === -1) {
        throw new Error("There was a problem while updating the notice");
      }

      commit(types.UPDATE_NOTICE, { index: index, notice: payload });
    },

    setRead({ commit, state }, payload) {
      const index = state.notices.findIndex((x) => x.id === payload);

      if (index === -1) {
        throw new Error("There was an unexpected error");
      }

      commit(types.SET_READ, index);
    },

    deleteNotice({ commit, state }, payload) {
      const index = state.notices.findIndex((x) => x.id === payload);

      if (index === -1) {
        throw new Error("There was a problem while deleting the notice");
      }

      commit(types.DELETE_NOTICE, index);
    },
  },
};
