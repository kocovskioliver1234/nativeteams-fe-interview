import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

// import upperFirst from "lodash/upperFirst";
// import camelCase from "lodash/camelCase";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import Vuelidate from "vuelidate";

// Import Bootstrap and BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

// Import moment js
import moment from "moment";
Vue.prototype.moment = moment;

// Import notification plugin
import notification from "./plugins/notification";
Vue.use(notification);

// Custom CSS
import "~/assets/styles/main.css";
import "~/assets/styles/responsive.css";

// Custom Icons
import "~/assets/fonts/native-teams-icons/style.css";

// GLOBAL REGISTER COMPONENTS

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

// const requireComponent = require.context(
//   "./components/BaseComponents",
//   false,
//   /Base[A-Z]\w+\.(vue|js)$/
// );

// requireComponent.keys().forEach((fileName) => {
//   const componentConfig = requireComponent(fileName);

//   const componentName = upperFirst(
//     camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, "$1"))
//   );

//   Vue.component(componentName, componentConfig.default || componentConfig);
// });

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
