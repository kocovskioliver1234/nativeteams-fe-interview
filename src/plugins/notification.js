import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-bootstrap.css";
import Vue from "vue";
Vue.use(VueToast);

export default {
  install(Vue) {
    /**
     * Show Error notificatons  the application
     * @param title
     * @param text
     */
    Vue.prototype.toast = function (
      variant = null,
      text,
      title = null,
      timeout = null
    ) {
      if (title === null) {
        switch (variant) {
          case "success":
            title = "Success";
            break;
          case "error":
            title = "Error";
            break;
          case "warning":
            title = "Warning";
            break;
          case "default":
            title = "Notification";
            break;
          case "info":
            title = "Information";
            break;
        }
      }

      this.$toast.open({
        message: text,
        type: variant,
        duration: timeout ? timeout : 3000,
        dismissible: true,
      });
    };
  },
};
